Collabora theme for Darkslide
=============================

This theme has been tested with Darkslide 2.3.3 and 4.0.1.

Generating HTML slides
----------------------

Make sure you have Darkslide installed, then run `make`.
This will also generate a symlink called `index.html`, making it easier to
publish the slides online.

Choosing 16:9 vs 4:3 aspect ratio
---------------------------------

To use 16:9 aspect ratio, put `theme = collabora-169` into your `slides.cfg`:

    [darkslide]
    source = slides.md
    theme = collabora-169
    # css = slides.css
    destination = slides.html

For 4:3, use `theme = collabora-43`.

Extras
------

To put photo as the background of the title slide, enable the custom CSS
(`css = slides.css`) and put something like this into it:

    .slide-1 {
        background-image: url('path/to/the/photo.jpg');
        background-size: cover;
    }

Similarly, to put *We’re hiring* slide as the final one, add:

    .slide- {
        background: url('common/hiring.svg');
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        background-color: #5c3ccd;
    }
    
    .slide-::before {
        content: none;
    }
    
    .slide- footer {
        background: none;
        border: 0;
    }
